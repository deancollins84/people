<?php

require_once "vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use deancollins84\people\DataSeed;
use Exception;

class DataSeedTest extends TestCase {
    
    protected $_dataSeed;
    
    public function setUp(){
        $this->_dataSeed = new DataSeed;
    }

    public function testDataIsNotJson() {
        try {
            $this->_dataSeed->load(file_get_contents('src/DbMigration.php'));
        } catch (Exception $exception) {
            return;
        }
        $this->fail('Valid json required.');
    }

    public function testDataIsJson() {
        $this->assertTrue($this->_dataSeed->load(file_get_contents('src/people.json')));
    }
    
    public function testPopulate(){
        $this->_dataSeed->load(file_get_contents('src/people.json'));
        $this->_dataSeed->populate();
    }

}
