<?php

require_once "vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use deancollins84\people\DbMigration;
use PDOException;

class DbMigrationTest extends TestCase {

    public function testDatabaseCreation() {
        try {
            new DbMigration();
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }

}
