<?php

namespace deancollins84\people\interfaces;

use deancollins84\people\Person;

interface PeopleRepoInterface {

     public function push(Person $person);
     
     public function getAll(int $limit = 10, int $offset = null, string $orderBy =null);
     
     public function getAllByEyeColour(string $colour);
     
     public function getAllByGender(string $gender = 'male');
     
     public function getBreakdownByAge(int $age);
     
     public function search(string $searchTerm);

}
