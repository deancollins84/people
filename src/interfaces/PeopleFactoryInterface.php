<?php

namespace deancollins84\people\interfaces;

interface PeopleFactoryInterface {

    public function createPerson(string $name, int $age, string $id = null);

}
