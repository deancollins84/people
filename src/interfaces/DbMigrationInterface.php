<?php

namespace deancollins84\people\interfaces;

interface DbMigrationInterface {

    public function createGenderTable();

    public function createEyeColourTable();

    public function createAddressTable();

    public function createTagTable();

    public function createPeopleTable();

    public function createPeopleTagTable();
}
