<?php

namespace deancollins84\people\interfaces;

interface ApiInterface {

    public function returnAll(int $offset);

    public function applyLimit(int $limit);

    public function applyOrder(string $field, string $by = 'asc');

    public function applyFilter(string $field, string $value);
    
    public function fuzzySearch(string $searchTerm);
}
