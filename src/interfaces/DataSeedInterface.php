<?php

namespace deancollins84\people\interfaces;

interface DataSeedInterface {

    public function load(string $jsonString) : bool;
}
