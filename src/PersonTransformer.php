<?php

namespace deancollins84\people;

require "vendor/autoload.php";

use deancollins84\people\Person;
use League\Fractal;

class PersonTransformer extends Fractal\TransformerAbstract {

    public function transform(Person $person) {
        $person = [
            'id' => (string) $person->getId(),
            'name' => (string) $person->getName(),
            'age' => (int) $person->getAge(),
        ];
       
        return json_encode($person);
    }

}
