<?php

namespace deancollins84\people;

class Person {

    protected $_id;
    protected $_name;
    protected $_age;
    
    public function setId($id){
        $this->_id = $id;
    }
    
    public function setName($name){
        $this->_name = $name;
    }
    
    public function setAge($age){
        $this->_age = $age;
    }
    
    public function getId(){
        return $this->_id;
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function getAge(){
        return $this->_age;
    }
    
}
