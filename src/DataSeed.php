<?php

namespace deancollins84\people;

require "vendor/autoload.php";

use deancollins84\people\interfaces\DataSeedInterface;
use deancollins84\people\PeopleFactory;
use deancollins84\people\PeopleRepo;
use Exception;

class DataSeed implements DataSeedInterface {

    protected $_json = false;
    
    public function load(string $jsonString) : bool {
        if($this->isJson($jsonString)){
            $this->_json = $jsonString;
            return true;
        }
        return false;
    }

    protected function isJson(string $string) : bool {
        json_decode($string);
        if(json_last_error() != JSON_ERROR_NONE) {
            throw new Exception ('Not valid json.'); 
        }
        return true;
    }
    
    public function populate(){
        if($this->_json){
            $peopleArray = json_decode($this->_json, true);
        
            $peopleFactory = new PeopleFactory;
            $peopleRepo = new PeopleRepo;
            
        foreach($peopleArray as $person)
        {
            if($person = $peopleFactory->createPerson($person['name'], $person['age'], $person['_id'])){
                $peopleRepo->push($person);
            }
        }

        
        
        
        }
    }
    



}
