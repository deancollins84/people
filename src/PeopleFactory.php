<?php

namespace deancollins84\people;

require "vendor/autoload.php";

use deancollins84\people\interfaces\PeopleFactoryInterface;
use deancollins84\people\Person;

class PeopleFactory implements PeopleFactoryInterface {

    public function createPerson(string $name, int $age, string $id = null){
        if(!is_null($id)){
            $person = new Person;
            $person->setId($id);
            $person->setName($name);
            $person->setAge($age);
            return $person;
        }
        return false;
    }
}
