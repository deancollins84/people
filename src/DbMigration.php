<?php

namespace deancollins84\people;

require "vendor/autoload.php";

use deancollins84\people\interfaces\DbMigrationInterface;
use PDO;

class DbMigration implements DbMigrationInterface {

    protected $_host = '192.168.10.10';
    protected $_root = 'homestead';
    protected $_password = 'secret';
    protected $_database = 'people';
    protected $_pdoDb;

    public function __construct($host = false, $root = false, $password = false) {
        $this->_pdoDb = new PDO('mysql:host=' . $host ? : $this->_host, $root ? : $this->_root, $password ? : $this->_password);
        $this->_pdoDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_pdoDb->exec("CREATE DATABASE IF NOT EXISTS `$this->_database`; use `$this->_database`;");
        $this->createGenderTable()->createEyeColourTable()->createAddressTable()->createTagTable()->createPeopleTable()->createPeopleTagTable();
    }

    public function createGenderTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `genders` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `type` varchar(12) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

    public function createEyeColourTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `eye_colours` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `colour` varchar(30) DEFAULT NULL,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `colour_UNIQUE` (`colour`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

    public function createAddressTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `addresses` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `address` varchar(45) DEFAULT NULL,
                `latitude` float(10,6) DEFAULT NULL,
                `longitude` float(10,6) DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

    public function createTagTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `tags` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(45) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `tag` (`name`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

    public function createPeopleTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `people` (
                `id` varChar(150) NOT NULL,
                `gender_id` int(11) DEFAULT NULL,
                `address_id` int(11) DEFAULT NULL,
                `eye_colour_id` int(11) DEFAULT NULL,
                `is_active` tinyint(2) DEFAULT '0',
                `image` varchar(250) DEFAULT NULL,
                `age` tinyint(8) DEFAULT NULL,
                `name` varchar(100) DEFAULT NULL,
                `email` varchar(120) DEFAULT NULL,
                `phone` varchar(80) DEFAULT NULL,
                `about` blob,
                `registered` datetime DEFAULT NULL,
                `balance` varchar(80) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `fk_people_gender_idx` (`gender_id`),
                KEY `fk_people_address_idx` (`address_id`),
                KEY `fk_people_eyes_idx` (`eye_colour_id`),
                CONSTRAINT `fk_people_address` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                CONSTRAINT `fk_people_eyes` FOREIGN KEY (`eye_colour_id`) REFERENCES `eye_colours` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                CONSTRAINT `fk_people_gender` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

    public function createPeopleTagTable() {
        $this->_pdoDb->exec("CREATE TABLE IF NOT EXISTS `people_tags` (
            `id` int(11) NOT NULL,
            `person_id` int(11) DEFAULT NULL,
            `tag_id` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_person_id_idx` (`person_id`),
            KEY `fk_tag_id_idx` (`tag_id`),
            CONSTRAINT `fk_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return $this;
    }

}
