<?php

namespace deancollins84\people;

require "vendor/autoload.php";

use deancollins84\people\interfaces\PeopleRepoInterface;
use deancollins84\people\Person;
use PDO;

use deancollins84\people\PersonTransformer;
use League\Fractal;

class PeopleRepo implements PeopleRepoInterface {

    protected $_host = '192.168.10.10';
    protected $_root = 'homestead';
    protected $_password = 'secret';
    protected $_database = 'people';
    protected $_pdoDb;

    public function __construct($host = false, $root = false, $password = false) {
        $this->_pdoDb = new PDO('mysql:host=' . $host ? : $this->_host, $root ? : $this->_root, $password ? : $this->_password);
        $this->_pdoDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_pdoDb->query("use `$this->_database`;");
    }

    public function push(Person $person) {
        $query = $this->_pdoDb->prepare('
            INSERT INTO people
                (id, name, age) 
            VALUES 
                (:id, :name, :age)
            ON DUPLICATE KEY
                UPDATE name=:name, age=:age;                
            ');

        $id = $person->getId();
        $name = $person->getName();
        $age = $person->getAge();

        $query->bindParam(':id', $id);
        $query->bindParam(':name', $name);
        $query->bindParam(':age', $age);

        return $query->execute();
    }

    public function getAll(int $limit = 10, int $offset = null, string $orderBy = null) {
        $query = $this->_pdoDb->prepare('SELECT * FROM people');
        
        if(!is_null($offset)){
            
        }
        
        if(!is_null($orderBy)){
            
        }

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, Person::class);
        $people = $query->fetchAll();
        
        return new Fractal\Resource\Collection($people, new PersonTransformer);
    }

    public function getAllByEyeColour(string $colour) {
        
    }

    public function getAllByGender(string $gender = 'male') {
        
    }

    public function getBreakdownByAge(int $age) {
        
    }

    public function search(string $searchTerm) {
        
    }

}
